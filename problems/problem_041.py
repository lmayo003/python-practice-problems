# Complete the add_csv_lines function which accepts a list
# as its only parameter. Each item in the list is a
# comma-separated string of numbers. The function should
# return a new list with each entry being the corresponding
# sum of the numbers in the comma-separated string.
#
# These kinds of strings are called CSV strings, or comma-
# sepearted values strings.
#
# Examples:
#   * input:  []
#     output: []
#   * input:  ["3", "1,9"]
#     output: [3, 10]
#   * input:  ["8,1,7", "10,10,10", "1,2,3"]
#     output:  [16, 30, 6]
#
# Look up the string split function to find out how to
# split a string into pieces.

# Write out your own pseudocode to help guide you.

def add_csv_lines(csv_lines):
    result_list = [] #return new list
    for item in csv_lines: #item is the name of the value in the list of values cvs_line
        pieces = item.split(",") #piece is the name of the new variable in new list, pieces is the name of the new variable list
        line_sum = 0 #line_sum is what we're doing to the old list to make it a new list. we have to start from 0
        for piece in pieces: #for the variables in the list of variables
            value = int(piece) #value is the name of the number value of the string piece
            line_sum += value #this is what we're doing. the actual function
        result_list.append(line_sum) #and adfter we're done, we're adding the new value to the new list after the math