# Complete the remove_duplicate_letters that takes a string
# parameter "s" and returns a string with all of the
# duplicates removed.
#
# Examples:
#   * For "abc", the result is "abc"
#   * For "abcabc", the result is "abc"
#   * For "abccba", the result is "abc"
#   * For "abccbad", the result is "abcd"
#
# If the list is empty, then return the empty string.

letters1 = ['a', 'b', 'c', 'a', 'b', 'c']
letters2 = ['a', 'b', 'c', 'c', 'b', 'a']
letters3 = ['a', 'b', 'c', 'c', 'b', 'a', 'd']

print("Original List: ", letters1, letters2, letters3)
res1= [*set(letters1)]
res2= [*set(letters2)]
res3= [*set(letters3)]
print("List after duplicates", res1, res2, res3)
