# Write a class that meets these requirements.
#
# Name:       BankAccount
#
# Required state:
#    * opening balance, the amount of money in the bank account
#
# Behavior:
#    * get_balance()      # Returns how much is in the bank account
#    * deposit(amount)    # Adds money to the current balance
#    * withdraw(amount)   # Reduces the current balance by amount
#
# Example:
#    account = BankAccount(100)
#
#    print(account.get_balance())  # prints 100
#    account.withdraw(50)
#    print(account.get_balance())  # prints 50
#    account.deposit(120)
#    print(account.get_balance())  # prints 170
#
# There is pseudocode for you to guide you.

# class BankAccount
    # method initializer(self, balance)
        # self.balance = balance

    # method get_balance(self)
        # returns the balance

    # method withdraw(self, amount)
        # reduces the balance by the amount

    # method deposit(self, amount)
        # increases the balance by the amount
class BankAccount:
    def __init__ (self, g_balance):
        self.g_balance = g_balance
    def balance (self):
        return  self.g_balance
    def withdraw (self, amount):
        if amount > self.g_balance:
            return "Insufficient Funds"
        else: 
            self.g_balance -= amount
            return self.balance
        
    def deposit (self, amount):
        self.amount = amount
        self.g_balance += self.amount
        return self.g_balance

checkingacct = BankAccount(100)
print(checkingacct.balance())
print(checkingacct.withdraw(250))
print(checkingacct.deposit(100))