# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you

def calculate_average(*values):
    if sum(values) <= 0:
        return None
    elif sum(values) >= 0:
        return sum(values)/len(values)
Grade = (1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
print(Grade)