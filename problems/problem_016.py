# Complete the is_inside_bounds function which takes an x
# coordinate and a y coordinate, and then tests each to
# make sure they're between 0 and 10, inclusive.

def is_inside_bounds(x, y):
    if x in range(1,10) and y in range(1,10):
        return True
    elif x in range(1,10) and y not in range(1,10):
        return "X is in range, but Y is not"
    elif x not in range(1,10) and y in range(1,10):
        return "X is not in range, but Y is"
    else: False
x = 9
y = 12

print(is_inside_bounds(x,y))