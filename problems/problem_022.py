# Complete the gear_for_day function which returns a list of
# gear needed for the day given certain criteria.
#   * If the day is not sunny and it is a workday, the list
#     needs to contain "umbrella"
#   * If it is a workday, the list needs to contain "laptop"
#   * If it is not a workday, the list needs to contain
#     "surfboard"

def gear_for_day(Day, Weather):
    if Day in workday and Weather in bad_weather:
        return work_1
    elif Day in workday and Weather in good_weather:
        return work_2
    elif Day in weekend and Weather in good_weather:
        return fun_1
    else: return fun_2
workday = ["Monday", "Tuesday", "wednesday", "Thursday", "Friday"]
weekend = ["Saturday", "Sunday"]
good_weather = ["Sunny", "Cloudy", "Windy"]
bad_weather = ["Rain", "Hail", "Sleet", "Snow"]
work_1 = ["Laptop", "Umbrella"]
work_2= ["Laptop", "Smile"]
fun_1 = ["Surfboard", "Good Vibes", "Phone off"]
fun_2 = ["Sew"]

print(gear_for_day("Tuesday", "Sunny"))