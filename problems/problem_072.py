# Write a class that meets these requirements.
#
# Name:       Person
#
# Required state:
#    * name, a string
#    * hated foods list, a list of names of food they don't like
#    * loved foods list, a list of names of food they really do like
#
# Behavior:
#    * food_list(food name)  * returns None if the food name is not in their
#                                  hated or loved food lists
#                        * returns True if the food name is in their
#                                  loved food list
#                        * returns False if the food name is in their
#                                  hated food list
#
# Example:
#    person = Person("Malik",
#                    ["cottage cheese", "sauerkraut"],
#                    ["pizza", "schnitzel"])
#
#    print(person.food_list("lasagna"))     # Prints None, not in either list
#    print(person.food_list("sauerkraut"))  # Prints False, in the hated list
#    print(person.food_list("pizza"))       # Prints True, in the loved list
#
# Do it without pseudocode, this time, from memory. Don't look
# at the last one you just wrote unless you really must.


class Person:
    def __init__(self,name, loved_food, hated_food):
        self.name = name
        self.loved_food = loved_food
        self.hated_food = hated_food
    def food_list(self, food):
        if food in self.loved_food:
            return True
        elif food in self.hated_food:
            return False
        else: return None
person = Person('Luis', ['apples', 'bananas', 'cherries', 'donuts', 'pizza'], 
                ['elephants', 'fridge', 'giraffes', 'helicopters', 'sauerkraut'])
print(person.food_list("lasagna"))     
print(person.food_list("sauerkraut"))  
print(person.food_list("pizza"))       